# Get global settings: GET /rest/api/3/configuration
# Returns the global settings in Jira. 
# These settings determine whether optional features 
# (for example, subtasks, time tracking, and others) are enabled. 
# If time tracking is enabled, this operation also returns the time tracking configuration.

# This code sample uses the 'requests' library:
# http://docs.python-requests.org
import requests
from requests.auth import HTTPBasicAuth
import json

url = "/rest/api/3/configuration"

auth = HTTPBasicAuth("email@example.com", "<api_token>")

headers = {
   "Accept": "application/json"
}

response = requests.request(
   "GET",
   url,
   headers=headers,
   auth=auth
)

print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))